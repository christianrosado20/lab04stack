package menuClasses;

import dataManager.DMComponent;
import ioManagementClasses.IOComponent;

public class DeleteListAction implements Action {

	@Override
	public void execute(Object args) {
		// TODO Auto-generated method stub
		DMComponent dm = (DMComponent) args; 
		IOComponent io = IOComponent.getComponent(); 
		io.output("\nDeleting an existing list of integers:\n"); 
		String listName = io.getInput("\nEnter name of the list you wish to delete: "); 
		dm.removeList(listName);
	}

}
